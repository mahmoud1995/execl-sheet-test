
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class testEmpName {

    @Test
    public void testFullName() throws  IOException {

        List<String> names=new ArrayList<String>();

        File file = new File(System.getProperty("user.dir")+"/resources/testEpmName.xlsx");

        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook testWorkBook=new XSSFWorkbook(inputStream);
        XSSFSheet sheet = testWorkBook.getSheetAt(0);
        DataFormatter objDefaultFormat = new DataFormatter();
        FormulaEvaluator objFormulaEvaluator = new XSSFFormulaEvaluator( testWorkBook);

        Iterator<Row> rowItrator = sheet.rowIterator();
        rowItrator.next();

        while (rowItrator.hasNext()) {
            for( Cell cell : rowItrator.next()){
                objFormulaEvaluator.evaluate(cell);
                String cellValue = objDefaultFormat.formatCellValue(cell, objFormulaEvaluator);
                names.add(cellValue);
            }
            System.out.println(names.get(0) + names.get(1) + "=" + names.get(2));

            Assert.assertEquals(names.get(0) + names.get(1) , names.get(2));
        }
    }
}
